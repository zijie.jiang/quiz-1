package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private String should_return_200_when_create_game() throws Exception {
        ResultActions actions = mockMvc.perform(post("/api/games"));
        actions.andExpect(status().isCreated())
                .andExpect(header().exists("location"));
        return actions.andReturn().getResponse().getHeader("location");
    }

    @Test
    void should_return_200_when_find_game_by_id() throws Exception {
        String url = should_return_200_when_create_game();
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.answer").exists());
    }

    @Test
    void should_return_200_when_post_answer() throws Exception {
        String url = should_return_200_when_create_game();
        mockMvc.perform(patch(url)
                .contentType(APPLICATION_JSON)
                .content("{ \"answer\": \"1234\" }"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(false))
                .andExpect(content().contentType(APPLICATION_JSON));
    }

    @Test
    void should_return_404_when_game_id_is_not_exist() throws Exception {
        mockMvc.perform(get("/api/games/100"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_400_given_not_correct_format_game_id() throws Exception {
        mockMvc.perform(patch("/api/games/a"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_given_answer_contains_char() throws Exception {
        mockMvc.perform(patch("/api/games/1")
                .contentType(APPLICATION_JSON)
                .content("{ \"answer\": \"1bcd\" }"))
                .andExpect(status().isBadRequest());
    }
}
