package com.twuc.webApp.services;

import com.twuc.webApp.model.Answer;
import com.twuc.webApp.model.GameRealAnswer;
import com.twuc.webApp.model.Hint;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class GameService {

    public Hint check(Answer answer, GameRealAnswer gameRealAnswer) {
        AtomicInteger a = new AtomicInteger(0);
        AtomicInteger b = new AtomicInteger(0);
        AtomicInteger index = new AtomicInteger(0);
        int[] ans = new int[4];
        int[] realAns = gameRealAnswer.getAnswer();

        for (int i = 0; i < answer.getAnswer().length(); i++) {
            ans[i] = (int) answer.getAnswer().charAt(i) - 48;
        }

        Arrays.stream(ans).forEach(_answer -> {
            if (_answer == realAns[index.getAndIncrement()]) {
                a.getAndIncrement();
            } else if (Arrays.stream(realAns).filter(an -> an == _answer).findFirst().isPresent()) {
                b.getAndIncrement();
            }
        });
        return new Hint(a.get() + "A" + b.get() + "B", a.get() == 4);

    }

    public GameRealAnswer findGameById(Integer gameId, List<GameRealAnswer> games) {
        Optional<GameRealAnswer> firstGame = games.stream()
                .filter(game -> game.getGameId().equals(gameId))
                .findFirst();
        if (firstGame.isPresent()) {
            return firstGame.get();
        }
        throw new RuntimeException("game is not exist");
    }

    public void checkAnswerFormat(Answer answer) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < answer.getAnswer().length(); i++) {
            int c = answer.getAnswer().charAt(i);
            if (c < 48 || c > 57) {
                throw new RuntimeException("answer format is error");
            }
            set.add(c);
        }
        if (set.size() != 4) {
            throw new RuntimeException("answer format is error");
        }
    }
}
