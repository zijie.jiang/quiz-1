package com.twuc.webApp.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity handleException(Exception e) {
        return e.getMessage().equals("game is not exist") ?
                ResponseEntity.notFound().build() :
                ResponseEntity.badRequest().build();
    }

}
