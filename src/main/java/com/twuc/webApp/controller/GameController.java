package com.twuc.webApp.controller;

import com.twuc.webApp.model.Answer;
import com.twuc.webApp.model.AnswerEntity;
import com.twuc.webApp.model.GameRealAnswer;
import com.twuc.webApp.model.Hint;
import com.twuc.webApp.services.GameService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RestController
@RequestMapping("/api/games")
@CrossOrigin
public class GameController {

    private final GameService service;
    private List<GameRealAnswer> games = new ArrayList<>();
    private int number;

    public GameController(GameService service) {
        this.service = service;
    }

    @PatchMapping("/{gameId}")
    public ResponseEntity<Hint> guess(@PathVariable Integer gameId,
                      @RequestBody @Valid Answer answer) {
        service.checkAnswerFormat(answer);
        GameRealAnswer gameRealAnswer = service.findGameById(gameId, games);
        return ResponseEntity.status(OK)
                .contentType(APPLICATION_JSON)
                .body(service.check(answer, gameRealAnswer));
    }

    @PostMapping
    public ResponseEntity<Void> createGame() {
        games.add(new GameRealAnswer(++number));
        return ResponseEntity.status(CREATED)
                .header("location", "/api/games/" + number)
                .header("Access-Control-Expose-Headers", "location")
                .body(null);
    }

    @GetMapping("/{gameId}")
    public ResponseEntity<AnswerEntity> findById(@PathVariable Integer gameId) {
        GameRealAnswer gameRealAnswer = service.findGameById(gameId, games);
        return ResponseEntity.status(OK)
                .contentType(APPLICATION_JSON)
                .body(new AnswerEntity().convert(gameRealAnswer));
    }
}
