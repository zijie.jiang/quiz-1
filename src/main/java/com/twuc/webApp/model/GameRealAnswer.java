package com.twuc.webApp.model;


import java.util.HashSet;
import java.util.Set;

public class GameRealAnswer {
    private Integer gameId;
    private int[] answer;

    public GameRealAnswer(Integer gameId) {
        this.gameId = gameId;
        generateNoRepeatAnswer();
    }

    public Integer getGameId() {
        return gameId;
    }

    public int[] getAnswer() {
        return answer;
    }

    private void generateNoRepeatAnswer() {
        this.answer = new int[4];
        Set<Integer> set = new HashSet<>();
        while(set.size() < 4) {
            set.add((int) (Math.random() * 10));
        }
        Object[] array = set.toArray();
        for (int i = 0; i < set.size(); i++) {
            this.answer[i] = (int) array[i];
        }
    }
}
