package com.twuc.webApp.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class Answer {

    @NotNull
    @Length(min = 4, max = 4)
    private String answer;

    public String getAnswer() {
        return answer;
    }
}
