package com.twuc.webApp.model;

public class Hint {

    private String hint;
    private boolean correct;

    public Hint() {

    }

    public Hint(String hint, Boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public String getHint() {
        return hint;
    }

    public boolean isCorrect() {
        return correct;
    }
}
