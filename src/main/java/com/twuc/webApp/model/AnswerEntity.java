package com.twuc.webApp.model;

import java.util.Arrays;

public class AnswerEntity {

    private Integer id;
    private String answer;

    public AnswerEntity convert(GameRealAnswer answer) {
        this.id = answer.getGameId();
        this.answer = Arrays.toString(answer.getAnswer());
        return this;
    }

    public Integer getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }
}
